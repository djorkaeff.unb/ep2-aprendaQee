# Aprenda+ QEE

# EP2 - OO 2017/02

Djorkaeff Alexandre Vilela Pereira - 16/0026822

Programa para simular cálculos de Engenharia de Energia.

O projeto consiste em um aplicativo desktop feito em Linguagem Java que servirá para os alunos da matéria de _Qualidade de Energia Elétrica_ do curso de _Engenharia de Energia_ da _Faculdade UnB-Gama_ onde deverão ser apresentados gráficos e resultados a partir de entradas dadas pelo usuário, sendo este aplicativo constituído por duas opções de simulação _(vide menu principal no início do programa)_ uma delas é simular um fluxo de potência fundamental e a outra é simular uma distorção harmônica, para maiores explicações quanto ao uso leia o tópico de __Utilização__.

# Instalação e Execução

O projeto foi criado e implementando usando a IDE Eclipse versão Oxygen.
[Eclipse Oxygen](https://projects.eclipse.org/releases/oxygen)

Para instalar o programa devemos dar clone neste repositório, em seguida descompactar a pasta e abrir a mesma no eclipe.

Para executar o projeto devemos abrir o mesmo no _Eclipse_ e em seguida Executar a partir de src/view/Initial onde se encontra o método _main_.

# Utilização

## Tela Inicial
Ao iniciarmos o programa a tela que será apresentada é uma espécie de menu, com as duas opções de simulação disponíveis e um botão que pode ser usado para sair do programa:

![Tela inicial](https://uploaddeimagens.com.br/images/001/174/486/original/Captura_de_Tela_2017-11-15_a%CC%80s_15.48.57.png?1510768156)
Figura 1.1 - Menu Inicial

### Fluxo de potência Fundamental
Ao escolhermos a opção de Simular __Fluxo de potência Fundamental__ será apresentada a seguinte janela:
![Tela Fluxo de potência Fundamental](https://uploaddeimagens.com.br/images/001/174/491/full/Captura_de_Tela_2017-11-15_a%CC%80s_15.52.36.png?1510768398)
Figura 2.1 - Janela de simulação de Fluxo de potência Fundamental

Nesta tela deveremos colocar valores para __Veff__, __Aeff__, e para os __Ângulos__ respectivos, em seguida devemos clicar em __OK__ para que sejam gerados os gráficos de tensão, de corrente e de potência instantânea. Ao pressiornamos __OK__ o programa também calcula a _potência ativa_, _potência reativa_, _potência aparente_ e o _fator de potência_ e os apresenta na janela, para facilitar a visualização foi implementado um recurso adicional, ao clicarmos em um dos gráficos ele é apresentado em uma nova janela com os valores dispostos e uma grade para facilitar a visualização dos valores e o estudo do caso.

![Resultado Fluxo de potência Fundamental](https://uploaddeimagens.com.br/images/001/174/501/full/Captura_de_Tela_2017-11-15_a%CC%80s_16.00.43.png?1510768866)
Figura 2.2 - Resultado de teste em simulação de Fluxo de potência Fundamental

### Distorção Harmônica
Ao escolhermos a opção de Simular __Distorção Harmôncia__ será apresentada a seguinte janela:
![Tela Distorção Harmônica](https://uploaddeimagens.com.br/images/001/174/503/full/Captura_de_Tela_2017-11-15_a%CC%80s_16.02.43.png?1510768981)
Figura 3.1 - Janela de simulação de Distorção Harmônica

Nesta tela deveremos colocar valores para __Amplitude__ do componente fundamental, __Ângulo__ do componente fundamental, e em seguida selecionar se os harmônicos são Pares ou ímpares, logo depois selecionamos a quantidade de harmônicos que haverão na simulação, existe um scroll para os gráficos de harmônicos, os 6 gráficos são apresentados mas somente serão ativados os campos da quantidade de gráficos referente a seleção que foi feita previamente, depois disso devemos apertar o botão de __OK__ para liberar os gráficos de harmônicos, e em seguida preencher os campos de __Amplitude__ e __Ângulo__ de cada um dos gráficos de Harmônicos e pressionar em __Gerar Resultados__, e então serão apresentados os gráficos e a série de Fourier correspondente as entradas.

![Resultado Distorção Harmônica](https://uploaddeimagens.com.br/images/001/174/510/full/Captura_de_Tela_2017-11-15_a%CC%80s_16.08.32.png?1510769330)
Figura 3.2 - Resultado de teste em simulação de Distorção Harmônica.

