import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;

import model.CalculatePotency;

public class CalculatePotencyTest {

	public Double aeff = (double) 220;
	public Double veff = (double) 39;
	public Double angleVoltage = (double) 0;
	public Double angleElectricCurrent = (double) 35;
	public CalculatePotency calc;
	
	@Test
	public void test() {
		int resultVoltageActived;
		int resultVoltageReactived;
		int resultApparentPotency;
		Double resultPotencyFactor;
		
		resultVoltageActived = 7028;
		resultVoltageReactived = -4921;
		resultApparentPotency = 8580;
		resultPotencyFactor = (Double) 0.82;
		
		// Verificação
		assertEquals(resultVoltageActived, calc.voltageActived(aeff, veff, angleVoltage, angleElectricCurrent));	
		assertEquals(resultVoltageReactived,calc.voltageReactived(aeff, veff, angleVoltage, angleElectricCurrent));
		assertEquals(resultApparentPotency,calc.voltageApparent(aeff, veff));
	}

}

