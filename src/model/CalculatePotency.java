package model;

import java.util.ArrayList;
import java.util.List;

public class CalculatePotency{

	public static List<Double> scoresInit() {
		List<Double> scores = new ArrayList<>();
		Double value;
		
		for (double i=0; i<70; i=i+0.5) {
			value = (double) 0;
			scores.add((double) value);
		}
		
		return scores;
	}
	
	public static List<Double> scoresVoltage(Double veff, Double angle) {
		List<Double> scores = new ArrayList<>();
		Double value;
		
		for (double i=0; i<70; i=i+0.5) {
			if(veff<0 || veff>220) {
				value = (double) 0;
			}
			else if(angle < -180 || angle > 180) {
				value = (double) 0;
			}
			else if(veff==0 && angle==0) {
				value = (double) 0;
			}
			else {
				value = veff*Math.cos(Math.toRadians(2*Math.PI*60*i+angle));
			}
			scores.add((double) value);
		}
		
		return scores;
	}
	
	public static List<Double> scoresElectricCurrent(Double aeff, Double angle) {
		List<Double> scores = new ArrayList<>();
		Double value;
		for (double i=0; i<70; i=i+0.5) {
			if(aeff<0 || aeff>220) {
				value = (double) 0;
			}
			else if(angle < -180 || angle > 180) {
				value = (double) 0;
			}
			else if(aeff==0 && angle==0) {
				value = (double) 0;
			}
			else {
				value = aeff*Math.cos(Math.toRadians(2*Math.PI*60*i+angle));
			}
			scores.add((double) value);
		}
		
		return scores;
	}
	
	public static List<Double> scoresResult(List<Double> voltage, List<Double> electricCurrent) {
		List<Double> scores = new ArrayList<>();
		
		Double value;
		for (int i=0; i<70; i=i+1) {
			value = voltage.get(i) * electricCurrent.get(i);
			scores.add((double) value);
		}
		
		return scores;	
	}
	
	public static int voltageActived(Double aeff, Double veff, Double angleVoltage, Double angleElectricCurrent) {
		int result;
		
		result = (int) (aeff * veff * Math.cos(Math.toRadians(angleVoltage - angleElectricCurrent)));
		
		return result;
	}
	
	public static int voltageReactived(Double aeff, Double veff, Double angleVoltage, Double angleElectricCurrent) {
		int result;
		
		result = (int) (aeff * veff * Math.sin(Math.toRadians(angleVoltage - angleElectricCurrent)));
		
		return result;
	}
	
	public static int voltageApparent(Double aeff, Double veff) {
		int result;
		
		result = (int) (aeff * veff);
		
		return result;
	}
	
	public static double factorVoltage(Double angleVoltage, Double angleElectricCurrent) {
		double value = 0;
		
		if(angleVoltage==0 && angleElectricCurrent==0)
			return value;
		
		if(angleVoltage==angleElectricCurrent)
			return (double) 1;
		
		value = Math.cos(Math.toRadians(angleVoltage - angleElectricCurrent));
		
		return value;
	}
}