package model;

import java.util.ArrayList;
import java.util.List;

public class CalculateDistortion {
	public static List<Double> scoresInit() {
		List<Double> scores = new ArrayList<>();
		Double value;
		
		for (double i=0; i<70; i=i+0.5) {
			value = (double) 0;
			scores.add((double) value);
		}
		
		return scores;
	}
	
	public static List<Double> scoresFundamental(Double veff, Double angle) {
		List<Double> scores = new ArrayList<>();
		Double value;
		
		for (double i=0; i<70; i=i+0.5) {
			if(veff<0 || veff>220) {
				value = (double) 0;
			}
			else if(angle < -180 || angle > 180) {
				value = (double) 0;
			}
			else if(veff==0 && angle==0) {
				value = (double) 0;
			}
			else {
				value = veff*Math.cos(Math.toRadians(2*Math.PI*60*i+angle));
			}
			scores.add((double) value);
		}
		
		return scores;
	}
	
	public static List<Double> scoresHarmonic(Double aeff, Double angle, int h) {
		List<Double> scores = new ArrayList<>();
		Double value;
		for (double i=0; i<70; i=i+0.5) {
			if(aeff<0 || aeff>220) {
				value = (double) 0;
			}
			else if(angle < -180 || angle > 180) {
				value = (double) 0;
			}
			else if(aeff==0 && angle==0) {
				value = (double) 0;
			}
			else {
				value = aeff*Math.cos(Math.toRadians(h*2*Math.PI*60*i+angle));
			}
			scores.add((double) value);
		}
		
		return scores;
	}
	
	public static List<Double> scoresResult(Double amplitudeFundamental, Double angleFundamental, Double amplitude1, int order1, Double angle1,
			Double amplitude2, int order2, Double angle2, Double amplitude3, int order3, Double angle3,
			Double amplitude4, int order4, Double angle4, Double amplitude5, int order5, Double angle5,
			Double amplitude6, int order6, Double angle6) {
		
		List<Double> scores = new ArrayList<>();
		
		Double value;
		for (double i=0; i<70; i=i+0.5) {
			value = amplitudeFundamental*Math.cos(Math.toRadians(2*Math.PI*60*i+angleFundamental));
			if(amplitude1!=0) {
				value = value + amplitude1*Math.cos(Math.toRadians(order1*2*Math.PI*60*i+angle1));
			}
			if(amplitude2!=0) {
				value = value + amplitude2*Math.cos(Math.toRadians(order2*2*Math.PI*60*i+angle2));
			}
			if(amplitude3!=0) {
				value = value + amplitude3*Math.cos(Math.toRadians(order3*2*Math.PI*60*i+angle3));
			}
			if(amplitude4!=0) {
				value = value + amplitude4*Math.cos(Math.toRadians(order4*2*Math.PI*60*i+angle4));
			}
			if(amplitude5!=0) {
				value = value + amplitude5*Math.cos(Math.toRadians(order5*2*Math.PI*60*i+angle5));
			}
			if(amplitude6!=0) {
				value = value + amplitude6*Math.cos(Math.toRadians(order6*2*Math.PI*60*i+angle6));
			}
			scores.add((double) value);
		}
		
		return scores;	
	}
	
	public static String lblResult(Double amplitudeFundamental, Double angleFundamental, Double amplitude1, int order1, Double angle1,
			Double amplitude2, int order2, Double angle2, Double amplitude3, int order3, Double angle3,
			Double amplitude4, int order4, Double angle4, Double amplitude5, int order5, Double angle5,
			Double amplitude6, int order6, Double angle6) {
		
		String fourier = null;
		
		fourier = "f(t)=";
		fourier = fourier + String.format("%.0f",amplitudeFundamental) + "cos(ωt";
		if(angleFundamental>0 && angleFundamental!=0) {
			fourier = fourier + "+" + String.format("%.0f",angleFundamental) + "º" + ")+";
		}
		if(angleFundamental==0) {
			fourier = fourier + ")+";
		}
		if(angleFundamental<0) {
			fourier = fourier + String.format("%.0f",angleFundamental) + "º" + ")+";
		}
		
		
		if(amplitude1!=0) {
			fourier = fourier + String.format("%.0f", amplitude1) + "cos(" + 
					Integer.toString(order1) + "ωt";
			if(angle1>0) {
				fourier = fourier + "+";
			}
					
			fourier = fourier + String.format("%.0f",angle1) + "º)";
		}
		
		if(amplitude2!=0) {
			fourier = fourier + "+" + String.format("%.0f", amplitude2) + "cos(" + 
					Integer.toString(order2) + "ωt";
			if(angle2>0) {
				fourier = fourier + "+";
			}
					
			fourier = fourier + String.format("%.0f",angle2) + "º)";
		}
		
		if(amplitude3!=0) {
			fourier = fourier + "+" + String.format("%.0f", amplitude3) + "cos(" + 
					Integer.toString(order3) + "ωt";
			if(angle3>0) {
				fourier = fourier + "+";
			}
					
			fourier = fourier + String.format("%.0f",angle3) + "º)";
		}
		
		if(amplitude4!=0) {
			fourier = fourier + "+" + String.format("%.0f", amplitude4) + "cos(" + 
					Integer.toString(order4) + "ωt";
			if(angle4>0) {
				fourier = fourier + "+";
			}
					
			fourier = fourier + String.format("%.0f",angle4) + "º)";
		}
		
		if(amplitude5!=0) {
			fourier = fourier + "+" + String.format("%.0f", amplitude5) + "cos(" + 
					Integer.toString(order5) + "ωt"; 
			if(angle5>0) {
				fourier = fourier + "+";
			}
					
			fourier = fourier + String.format("%.0f",angle5) + "º)";
		}
		
		if(amplitude6!=0) {
			fourier = fourier + "+" + String.format("%.0f", amplitude6) + "cos(" + 
					Integer.toString(order6) + "ωt";
			
			if(angle6>0) {
				fourier = fourier + "+";
			}
					
			fourier = fourier + String.format("%.0f",angle6) + "º)";
		}
		
		return fourier;
	}
}
