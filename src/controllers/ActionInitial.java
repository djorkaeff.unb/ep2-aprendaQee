package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;

import view.HarmonicDistortion;
import view.FundamentalPower;
import view.HarmonicDistortion;

public class ActionInitial implements ActionListener {

	JFrame initial;
	
	public ActionInitial(JFrame initial) {
		this.initial = initial;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String comando = e.getActionCommand();
		
		if(comando.equals("PotFund")){
			initial.dispose();
			JFrame frame = new FundamentalPower();
			frame.setVisible(true);
		}
		
		if(comando.equals("DistHarm")) {
			initial.dispose();
			JFrame frame = new HarmonicDistortion();
			frame.setVisible(true);
		}
		
		else if(comando.equals("Exit")){
			initial.dispose();
			System.exit(0);
		}
	}

}
