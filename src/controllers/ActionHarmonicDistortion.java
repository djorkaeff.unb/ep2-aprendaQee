package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import model.CalculateDistortion;
import view.FundamentalPower;
import view.Graphic;
import view.HarmonicDistortion;
import view.PanelHarmonics;

public class ActionHarmonicDistortion implements ActionListener {

	PanelHarmonics harmonicDistortion;
	JSpinner spinner;
	JTextField txtAmplitudeFundComp;
	JTextField txtAngleFundComp;
	JLabel lblFinalResult;
	Graphic fundamentalComponent;
	Graphic result;
	
	public ActionHarmonicDistortion(PanelHarmonics harmonicDistortion, JSpinner spinner, Graphic fundamentalComponent, JTextField txtAmplitudeFundComp, JTextField txtAngleFundComp, JLabel lblFinalResult, Graphic result) {
		this.harmonicDistortion = harmonicDistortion;
		this.spinner = spinner;
		this.fundamentalComponent = fundamentalComponent;
		this.txtAmplitudeFundComp = txtAmplitudeFundComp;
		this.txtAngleFundComp = txtAngleFundComp;
		this.lblFinalResult = lblFinalResult;
		this.result = result;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String comando = e.getActionCommand();
		int nHarmonics = Integer.parseInt(spinner.getValue().toString());
		SpinnerNumberModel nHarm1 = new SpinnerNumberModel((Integer) 0,(Integer) 0, (Integer) 6,(Integer) 1);
		SpinnerNumberModel nHarm2 = new SpinnerNumberModel((Integer) 0,(Integer) 0, (Integer) 6,(Integer) 1);
		SpinnerNumberModel nHarm3 = new SpinnerNumberModel((Integer) 0,(Integer) 0, (Integer) 6,(Integer) 1);
		SpinnerNumberModel nHarm4 = new SpinnerNumberModel((Integer) 0,(Integer) 0, (Integer) 6,(Integer) 1);
		SpinnerNumberModel nHarm5 = new SpinnerNumberModel((Integer) 0,(Integer) 0, (Integer) 6,(Integer) 1);
		SpinnerNumberModel nHarm6 = new SpinnerNumberModel((Integer) 0,(Integer) 0, (Integer) 6,(Integer) 1);
		
		if(comando.equals("OkHarmonics")){
			result.setScores(CalculateDistortion.scoresInit());
			
			if(nHarmonics==1) {
				harmonicDistortion.txtAmplitude1.setEditable(true);
				harmonicDistortion.txtAngle1.setEditable(true);
				harmonicDistortion.spinner1.setModel(nHarm1);
				harmonicDistortion.txtAmplitude1.setText("0");
				harmonicDistortion.txtAngle1.setText("0");
				
				harmonicDistortion.txtAmplitude2.setEditable(false);
				harmonicDistortion.txtAngle2.setEditable(false);
				harmonicDistortion.spinner2.setModel(nHarm2);
				harmonicDistortion.txtAmplitude2.setText("0");
				harmonicDistortion.txtAngle2.setText("0");
				
				harmonicDistortion.txtAmplitude3.setEditable(false);
				harmonicDistortion.txtAngle3.setEditable(false);	
				harmonicDistortion.spinner3.setModel(nHarm3);
				harmonicDistortion.txtAmplitude3.setText("0");
				harmonicDistortion.txtAngle3.setText("0");
				
				harmonicDistortion.txtAmplitude4.setEditable(false);
				harmonicDistortion.txtAngle4.setEditable(false);
				harmonicDistortion.spinner4.setModel(nHarm4);
				harmonicDistortion.txtAmplitude4.setText("0");
				harmonicDistortion.txtAngle4.setText("0");
				
				harmonicDistortion.txtAmplitude5.setEditable(false);
				harmonicDistortion.txtAngle5.setEditable(false);
				harmonicDistortion.spinner5.setModel(nHarm5);
				harmonicDistortion.txtAmplitude5.setText("0");
				harmonicDistortion.txtAngle5.setText("0");
				
				harmonicDistortion.txtAmplitude6.setEditable(false);
				harmonicDistortion.txtAngle6.setEditable(false);
				harmonicDistortion.spinner6.setModel(nHarm6);
				harmonicDistortion.txtAmplitude6.setText("0");
				harmonicDistortion.txtAngle6.setText("0");
				
				JFormattedTextField tf1 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner1.getEditor()).getTextField();
				tf1.setEditable(false);
				JFormattedTextField tf2 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner2.getEditor()).getTextField();
				tf2.setEditable(false);
				JFormattedTextField tf3 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner3.getEditor()).getTextField();
				tf3.setEditable(false);
				JFormattedTextField tf4 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner4.getEditor()).getTextField();
				tf4.setEditable(false);
				JFormattedTextField tf5 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner5.getEditor()).getTextField();
				tf5.setEditable(false);
				JFormattedTextField tf6 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner6.getEditor()).getTextField();
				tf6.setEditable(false);
				
				harmonicDistortion.graphic1.setScores(CalculateDistortion.scoresInit());
				harmonicDistortion.graphic2.setScores(CalculateDistortion.scoresInit());
				harmonicDistortion.graphic3.setScores(CalculateDistortion.scoresInit());
				harmonicDistortion.graphic4.setScores(CalculateDistortion.scoresInit());
				harmonicDistortion.graphic5.setScores(CalculateDistortion.scoresInit());
				harmonicDistortion.graphic6.setScores(CalculateDistortion.scoresInit());
			}
			if(nHarmonics==2) {
				harmonicDistortion.txtAmplitude1.setEditable(true);
				harmonicDistortion.txtAngle1.setEditable(true);
				harmonicDistortion.spinner1.setModel(nHarm1);
				harmonicDistortion.txtAmplitude1.setText("0");
				harmonicDistortion.txtAngle1.setText("0");
				
				harmonicDistortion.txtAmplitude2.setEditable(true);
				harmonicDistortion.txtAngle2.setEditable(true);
				harmonicDistortion.spinner2.setModel(nHarm2);
				harmonicDistortion.txtAmplitude2.setText("0");
				harmonicDistortion.txtAngle2.setText("0");
				
				harmonicDistortion.txtAmplitude3.setEditable(false);
				harmonicDistortion.txtAngle3.setEditable(false);	
				harmonicDistortion.spinner3.setModel(nHarm3);
				harmonicDistortion.txtAmplitude3.setText("0");
				harmonicDistortion.txtAngle3.setText("0");
				
				harmonicDistortion.txtAmplitude4.setEditable(false);
				harmonicDistortion.txtAngle4.setEditable(false);
				harmonicDistortion.spinner4.setModel(nHarm4);
				harmonicDistortion.txtAmplitude4.setText("0");
				harmonicDistortion.txtAngle4.setText("0");
				
				harmonicDistortion.txtAmplitude5.setEditable(false);
				harmonicDistortion.txtAngle5.setEditable(false);
				harmonicDistortion.spinner5.setModel(nHarm5);
				harmonicDistortion.txtAmplitude5.setText("0");
				harmonicDistortion.txtAngle5.setText("0");
				
				harmonicDistortion.txtAmplitude6.setEditable(false);
				harmonicDistortion.txtAngle6.setEditable(false);
				harmonicDistortion.spinner6.setModel(nHarm6);
				harmonicDistortion.txtAmplitude6.setText("0");
				harmonicDistortion.txtAngle6.setText("0");
				
				harmonicDistortion.graphic1.setScores(CalculateDistortion.scoresInit());
				harmonicDistortion.graphic2.setScores(CalculateDistortion.scoresInit());
				harmonicDistortion.graphic3.setScores(CalculateDistortion.scoresInit());
				harmonicDistortion.graphic4.setScores(CalculateDistortion.scoresInit());
				harmonicDistortion.graphic5.setScores(CalculateDistortion.scoresInit());
				harmonicDistortion.graphic6.setScores(CalculateDistortion.scoresInit());
				
				JFormattedTextField tf1 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner1.getEditor()).getTextField();
				tf1.setEditable(false);
				JFormattedTextField tf2 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner2.getEditor()).getTextField();
				tf2.setEditable(false);
				JFormattedTextField tf3 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner3.getEditor()).getTextField();
				tf3.setEditable(false);
				JFormattedTextField tf4 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner4.getEditor()).getTextField();
				tf4.setEditable(false);
				JFormattedTextField tf5 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner5.getEditor()).getTextField();
				tf5.setEditable(false);
				JFormattedTextField tf6 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner6.getEditor()).getTextField();
				tf6.setEditable(false);
			}
			if(nHarmonics==3) {
				harmonicDistortion.txtAmplitude1.setEditable(true);
				harmonicDistortion.txtAngle1.setEditable(true);
				harmonicDistortion.spinner1.setModel(nHarm1);
				harmonicDistortion.txtAmplitude1.setText("0");
				harmonicDistortion.txtAngle1.setText("0");
				
				harmonicDistortion.txtAmplitude2.setEditable(true);
				harmonicDistortion.txtAngle2.setEditable(true);
				harmonicDistortion.spinner2.setModel(nHarm2);
				harmonicDistortion.txtAmplitude2.setText("0");
				harmonicDistortion.txtAngle2.setText("0");
				
				harmonicDistortion.txtAmplitude3.setEditable(true);
				harmonicDistortion.txtAngle3.setEditable(true);	
				harmonicDistortion.spinner3.setModel(nHarm3);
				harmonicDistortion.txtAmplitude3.setText("0");
				harmonicDistortion.txtAngle3.setText("0");
				
				harmonicDistortion.txtAmplitude4.setEditable(false);
				harmonicDistortion.txtAngle4.setEditable(false);
				harmonicDistortion.spinner4.setModel(nHarm4);
				harmonicDistortion.txtAmplitude4.setText("0");
				harmonicDistortion.txtAngle4.setText("0");
				
				harmonicDistortion.txtAmplitude5.setEditable(false);
				harmonicDistortion.txtAngle5.setEditable(false);
				harmonicDistortion.spinner5.setModel(nHarm5);
				harmonicDistortion.txtAmplitude5.setText("0");
				harmonicDistortion.txtAngle5.setText("0");
				
				harmonicDistortion.txtAmplitude6.setEditable(false);
				harmonicDistortion.txtAngle6.setEditable(false);
				harmonicDistortion.spinner6.setModel(nHarm6);
				harmonicDistortion.txtAmplitude6.setText("0");
				harmonicDistortion.txtAngle6.setText("0");
				
				harmonicDistortion.graphic1.setScores(CalculateDistortion.scoresInit());
				harmonicDistortion.graphic2.setScores(CalculateDistortion.scoresInit());
				harmonicDistortion.graphic3.setScores(CalculateDistortion.scoresInit());
				harmonicDistortion.graphic4.setScores(CalculateDistortion.scoresInit());
				harmonicDistortion.graphic5.setScores(CalculateDistortion.scoresInit());
				harmonicDistortion.graphic6.setScores(CalculateDistortion.scoresInit());
				
				JFormattedTextField tf1 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner1.getEditor()).getTextField();
				tf1.setEditable(false);
				JFormattedTextField tf2 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner2.getEditor()).getTextField();
				tf2.setEditable(false);
				JFormattedTextField tf3 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner3.getEditor()).getTextField();
				tf3.setEditable(false);
				JFormattedTextField tf4 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner4.getEditor()).getTextField();
				tf4.setEditable(false);
				JFormattedTextField tf5 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner5.getEditor()).getTextField();
				tf5.setEditable(false);
				JFormattedTextField tf6 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner6.getEditor()).getTextField();
				tf6.setEditable(false);
			}
			if(nHarmonics==4) {
				harmonicDistortion.txtAmplitude1.setEditable(true);
				harmonicDistortion.txtAngle1.setEditable(true);
				harmonicDistortion.spinner1.setModel(nHarm1);
				harmonicDistortion.txtAmplitude1.setText("0");
				harmonicDistortion.txtAngle1.setText("0");
				
				harmonicDistortion.txtAmplitude2.setEditable(true);
				harmonicDistortion.txtAngle2.setEditable(true);
				harmonicDistortion.spinner2.setModel(nHarm2);
				harmonicDistortion.txtAmplitude2.setText("0");
				harmonicDistortion.txtAngle2.setText("0");
				
				harmonicDistortion.txtAmplitude3.setEditable(true);
				harmonicDistortion.txtAngle3.setEditable(true);	
				harmonicDistortion.spinner3.setModel(nHarm3);
				harmonicDistortion.txtAmplitude3.setText("0");
				harmonicDistortion.txtAngle3.setText("0");
				
				harmonicDistortion.txtAmplitude4.setEditable(true);
				harmonicDistortion.txtAngle4.setEditable(true);
				harmonicDistortion.spinner4.setModel(nHarm4);
				harmonicDistortion.txtAmplitude4.setText("0");
				harmonicDistortion.txtAngle4.setText("0");
				
				harmonicDistortion.txtAmplitude5.setEditable(false);
				harmonicDistortion.txtAngle5.setEditable(false);
				harmonicDistortion.spinner5.setModel(nHarm5);
				harmonicDistortion.txtAmplitude5.setText("0");
				harmonicDistortion.txtAngle5.setText("0");
				
				harmonicDistortion.txtAmplitude6.setEditable(false);
				harmonicDistortion.txtAngle6.setEditable(false);
				harmonicDistortion.spinner6.setModel(nHarm6);
				harmonicDistortion.txtAmplitude6.setText("0");
				harmonicDistortion.txtAngle6.setText("0");
				
				harmonicDistortion.graphic1.setScores(CalculateDistortion.scoresInit());
				harmonicDistortion.graphic2.setScores(CalculateDistortion.scoresInit());
				harmonicDistortion.graphic3.setScores(CalculateDistortion.scoresInit());
				harmonicDistortion.graphic4.setScores(CalculateDistortion.scoresInit());
				harmonicDistortion.graphic5.setScores(CalculateDistortion.scoresInit());
				harmonicDistortion.graphic6.setScores(CalculateDistortion.scoresInit());
				
				JFormattedTextField tf1 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner1.getEditor()).getTextField();
				tf1.setEditable(false);
				JFormattedTextField tf2 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner2.getEditor()).getTextField();
				tf2.setEditable(false);
				JFormattedTextField tf3 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner3.getEditor()).getTextField();
				tf3.setEditable(false);
				JFormattedTextField tf4 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner4.getEditor()).getTextField();
				tf4.setEditable(false);
				JFormattedTextField tf5 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner5.getEditor()).getTextField();
				tf5.setEditable(false);
				JFormattedTextField tf6 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner6.getEditor()).getTextField();
				tf6.setEditable(false);
			}
			if(nHarmonics==5) {
				harmonicDistortion.txtAmplitude1.setEditable(true);
				harmonicDistortion.txtAngle1.setEditable(true);
				harmonicDistortion.spinner1.setModel(nHarm1);
				harmonicDistortion.txtAmplitude1.setText("0");
				harmonicDistortion.txtAngle1.setText("0");
				
				harmonicDistortion.txtAmplitude2.setEditable(true);
				harmonicDistortion.txtAngle2.setEditable(true);
				harmonicDistortion.spinner2.setModel(nHarm2);
				harmonicDistortion.txtAmplitude2.setText("0");
				harmonicDistortion.txtAngle2.setText("0");
				
				harmonicDistortion.txtAmplitude3.setEditable(true);
				harmonicDistortion.txtAngle3.setEditable(true);	
				harmonicDistortion.spinner3.setModel(nHarm3);
				harmonicDistortion.txtAmplitude3.setText("0");
				harmonicDistortion.txtAngle3.setText("0");
				
				harmonicDistortion.txtAmplitude4.setEditable(true);
				harmonicDistortion.txtAngle4.setEditable(true);
				harmonicDistortion.spinner4.setModel(nHarm4);
				harmonicDistortion.txtAmplitude4.setText("0");
				harmonicDistortion.txtAngle4.setText("0");
				
				harmonicDistortion.txtAmplitude5.setEditable(true);
				harmonicDistortion.txtAngle5.setEditable(true);
				harmonicDistortion.spinner5.setModel(nHarm5);
				harmonicDistortion.txtAmplitude5.setText("0");
				harmonicDistortion.txtAngle5.setText("0");
				
				harmonicDistortion.txtAmplitude6.setEditable(false);
				harmonicDistortion.txtAngle6.setEditable(false);
				harmonicDistortion.spinner6.setModel(nHarm6);
				harmonicDistortion.txtAmplitude6.setText("0");
				harmonicDistortion.txtAngle6.setText("0");
				
				harmonicDistortion.graphic1.setScores(CalculateDistortion.scoresInit());
				harmonicDistortion.graphic2.setScores(CalculateDistortion.scoresInit());
				harmonicDistortion.graphic3.setScores(CalculateDistortion.scoresInit());
				harmonicDistortion.graphic4.setScores(CalculateDistortion.scoresInit());
				harmonicDistortion.graphic5.setScores(CalculateDistortion.scoresInit());
				harmonicDistortion.graphic6.setScores(CalculateDistortion.scoresInit());
				
				JFormattedTextField tf1 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner1.getEditor()).getTextField();
				tf1.setEditable(false);
				JFormattedTextField tf2 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner2.getEditor()).getTextField();
				tf2.setEditable(false);
				JFormattedTextField tf3 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner3.getEditor()).getTextField();
				tf3.setEditable(false);
				JFormattedTextField tf4 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner4.getEditor()).getTextField();
				tf4.setEditable(false);
				JFormattedTextField tf5 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner5.getEditor()).getTextField();
				tf5.setEditable(false);
				JFormattedTextField tf6 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner6.getEditor()).getTextField();
				tf6.setEditable(false);
			}
			if(nHarmonics==6) {
				harmonicDistortion.txtAmplitude1.setEditable(true);
				harmonicDistortion.txtAngle1.setEditable(true);
				harmonicDistortion.spinner1.setModel(nHarm1);
				harmonicDistortion.txtAmplitude1.setText("0");
				harmonicDistortion.txtAngle1.setText("0");
				
				harmonicDistortion.txtAmplitude2.setEditable(true);
				harmonicDistortion.txtAngle2.setEditable(true);
				harmonicDistortion.spinner2.setModel(nHarm2);
				harmonicDistortion.txtAmplitude2.setText("0");
				harmonicDistortion.txtAngle2.setText("0");
				
				harmonicDistortion.txtAmplitude3.setEditable(true);
				harmonicDistortion.txtAngle3.setEditable(true);	
				harmonicDistortion.spinner3.setModel(nHarm3);
				harmonicDistortion.txtAmplitude3.setText("0");
				harmonicDistortion.txtAngle3.setText("0");
				
				harmonicDistortion.txtAmplitude4.setEditable(true);
				harmonicDistortion.txtAngle4.setEditable(true);
				harmonicDistortion.spinner4.setModel(nHarm4);
				harmonicDistortion.txtAmplitude4.setText("0");
				harmonicDistortion.txtAngle4.setText("0");
				
				harmonicDistortion.txtAmplitude5.setEditable(true);
				harmonicDistortion.txtAngle5.setEditable(true);
				harmonicDistortion.spinner5.setModel(nHarm5);
				harmonicDistortion.txtAmplitude5.setText("0");
				harmonicDistortion.txtAngle5.setText("0");
				
				harmonicDistortion.txtAmplitude6.setEditable(true);
				harmonicDistortion.txtAngle6.setEditable(true);
				harmonicDistortion.spinner6.setModel(nHarm6);
				harmonicDistortion.txtAmplitude6.setText("0");
				harmonicDistortion.txtAngle6.setText("0");
				
				harmonicDistortion.graphic1.setScores(CalculateDistortion.scoresInit());
				harmonicDistortion.graphic2.setScores(CalculateDistortion.scoresInit());
				harmonicDistortion.graphic3.setScores(CalculateDistortion.scoresInit());
				harmonicDistortion.graphic4.setScores(CalculateDistortion.scoresInit());
				harmonicDistortion.graphic5.setScores(CalculateDistortion.scoresInit());
				harmonicDistortion.graphic6.setScores(CalculateDistortion.scoresInit());
				
				JFormattedTextField tf1 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner1.getEditor()).getTextField();
				tf1.setEditable(false);
				JFormattedTextField tf2 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner2.getEditor()).getTextField();
				tf2.setEditable(false);
				JFormattedTextField tf3 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner3.getEditor()).getTextField();
				tf3.setEditable(false);
				JFormattedTextField tf4 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner4.getEditor()).getTextField();
				tf4.setEditable(false);
				JFormattedTextField tf5 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner5.getEditor()).getTextField();
				tf5.setEditable(false);
				JFormattedTextField tf6 = ((JSpinner.DefaultEditor) harmonicDistortion.spinner6.getEditor()).getTextField();
				tf6.setEditable(false);
			}
		}
		
		if(comando.equals("Result")) {
			Double fundamentalAmplitude;
			Double fundamentalAngle;
			Double amplitude1, amplitude2, amplitude3, amplitude4, amplitude5, amplitude6;
			Double angle1, angle2, angle3, angle4, angle5, angle6;
			Integer nHarmonics1, nHarmonics2, nHarmonics3, nHarmonics4, nHarmonics5, nHarmonics6;
			
			nHarmonics1 = Integer.parseInt(harmonicDistortion.spinner1.getValue().toString());
			nHarmonics2 = Integer.parseInt(harmonicDistortion.spinner2.getValue().toString());
			nHarmonics3 = Integer.parseInt(harmonicDistortion.spinner3.getValue().toString());
			nHarmonics4 = Integer.parseInt(harmonicDistortion.spinner4.getValue().toString());
			nHarmonics5 = Integer.parseInt(harmonicDistortion.spinner5.getValue().toString());
			nHarmonics6 = Integer.parseInt(harmonicDistortion.spinner6.getValue().toString());
			
			fundamentalAmplitude = Double.parseDouble(txtAmplitudeFundComp.getText());
			fundamentalAngle = Double.parseDouble(txtAngleFundComp.getText());
			
			amplitude1 = Double.parseDouble(harmonicDistortion.txtAmplitude1.getText());
			angle1 = Double.parseDouble(harmonicDistortion.txtAngle1.getText());
			
			amplitude2 = Double.parseDouble(harmonicDistortion.txtAmplitude2.getText());
			angle2 = Double.parseDouble(harmonicDistortion.txtAngle2.getText());
			
			amplitude3 = Double.parseDouble(harmonicDistortion.txtAmplitude3.getText());
			angle3 = Double.parseDouble(harmonicDistortion.txtAngle3.getText());
			
			amplitude4 = Double.parseDouble(harmonicDistortion.txtAmplitude4.getText());
			angle4 = Double.parseDouble(harmonicDistortion.txtAngle4.getText());
			
			amplitude5 = Double.parseDouble(harmonicDistortion.txtAmplitude5.getText());
			angle5 = Double.parseDouble(harmonicDistortion.txtAngle5.getText());
			
			amplitude6 = Double.parseDouble(harmonicDistortion.txtAmplitude6.getText());
			angle6 = Double.parseDouble(harmonicDistortion.txtAngle6.getText());
			
			if(angle1<-180 || angle2<-180 || angle3<-180 || angle4<-180 || angle5<-180 || angle6<-180) {
				JOptionPane.showMessageDialog(fundamentalComponent,
				        "Ângulo de Fase deve estar entre -180 e 180!", 
				        "Error", 
				        JOptionPane.ERROR_MESSAGE);
			}
			else if(angle1>180 || angle2>180 || angle3>180 || angle4>180 || angle5>180 || angle6>180) {
				JOptionPane.showMessageDialog(fundamentalComponent,
				        "Ângulo de Fase deve estar entre -180 e 180!", 
				        "Error", 
				        JOptionPane.ERROR_MESSAGE);
			}
			else if(amplitude1<0 || amplitude1>220) {
				JOptionPane.showMessageDialog(fundamentalComponent,
				        "Amplitude deve estar entre 0 e 220!", 
				        "Error", 
				        JOptionPane.ERROR_MESSAGE);
			}
			else if(amplitude2<0 || amplitude2>220) {
				JOptionPane.showMessageDialog(fundamentalComponent,
				        "Amplitude deve estar entre 0 e 220!", 
				        "Error", 
				        JOptionPane.ERROR_MESSAGE);
			}
			else if(amplitude3<0 || amplitude3>220) {
				JOptionPane.showMessageDialog(fundamentalComponent,
				        "Amplitude deve estar entre 0 e 220!", 
				        "Error", 
				        JOptionPane.ERROR_MESSAGE);
			}
			else if(amplitude4<0 || amplitude4>220) {
				JOptionPane.showMessageDialog(fundamentalComponent,
				        "Amplitude deve estar entre 0 e 220!", 
				        "Error", 
				        JOptionPane.ERROR_MESSAGE);
			}
			else if(amplitude5<0 || amplitude5>220) {
				JOptionPane.showMessageDialog(fundamentalComponent,
				        "Amplitude deve estar entre 0 e 220!", 
				        "Error", 
				        JOptionPane.ERROR_MESSAGE);
			}
			else if(amplitude6<0 || amplitude6>220) {
				JOptionPane.showMessageDialog(fundamentalComponent,
				        "Angle deve estar entre 0 e 220!", 
				        "Error", 
				        JOptionPane.ERROR_MESSAGE);
			}
			
			else if(amplitude1<0 || amplitude1>220) {
				JOptionPane.showMessageDialog(fundamentalComponent,
				        "Amplitude deve estar entre 0 e 220!", 
				        "Error", 
				        JOptionPane.ERROR_MESSAGE);
			}
			else if(amplitude2<0 || amplitude2>220) {
				JOptionPane.showMessageDialog(fundamentalComponent,
				        "Amplitude deve estar entre 0 e 220!", 
				        "Error", 
				        JOptionPane.ERROR_MESSAGE);
			}
			else if(amplitude3<0 || amplitude3>220) {
				JOptionPane.showMessageDialog(fundamentalComponent,
				        "Amplitude deve estar entre 0 e 220!", 
				        "Error", 
				        JOptionPane.ERROR_MESSAGE);
			}
			else if(amplitude4<0 || amplitude4>220) {
				JOptionPane.showMessageDialog(fundamentalComponent,
				        "Amplitude deve estar entre 0 e 220!", 
				        "Error", 
				        JOptionPane.ERROR_MESSAGE);
			}
			else if(amplitude5<0 || amplitude5>220) {
				JOptionPane.showMessageDialog(fundamentalComponent,
				        "Amplitude deve estar entre 0 e 220!", 
				        "Error", 
				        JOptionPane.ERROR_MESSAGE);
			}
			else if(amplitude6<0 || amplitude6>220) {
				JOptionPane.showMessageDialog(fundamentalComponent,
				        "Amplitude deve estar entre 0 e 220!", 
				        "Error", 
				        JOptionPane.ERROR_MESSAGE);
			}
			else{
			
				fundamentalComponent.setScores(CalculateDistortion.scoresFundamental(fundamentalAmplitude, fundamentalAngle));
			
				harmonicDistortion.graphic1.setScores(CalculateDistortion.scoresHarmonic(amplitude1, angle1, nHarmonics1));
				harmonicDistortion.graphic2.setScores(CalculateDistortion.scoresHarmonic(amplitude2, angle2, nHarmonics2));
				harmonicDistortion.graphic3.setScores(CalculateDistortion.scoresHarmonic(amplitude3, angle3, nHarmonics3));
				harmonicDistortion.graphic4.setScores(CalculateDistortion.scoresHarmonic(amplitude4, angle4, nHarmonics4));
				harmonicDistortion.graphic5.setScores(CalculateDistortion.scoresHarmonic(amplitude5, angle5, nHarmonics5));
				harmonicDistortion.graphic6.setScores(CalculateDistortion.scoresHarmonic(amplitude6, angle6, nHarmonics6));
			
				lblFinalResult.setText(CalculateDistortion.lblResult(fundamentalAmplitude, fundamentalAngle, amplitude1, nHarmonics1, angle1,
						amplitude2, nHarmonics2, angle2, amplitude3, nHarmonics3, angle3, amplitude4, nHarmonics4, angle4,
						amplitude5, nHarmonics5, angle5, amplitude6, nHarmonics6, angle6));
		
				result.setScores(CalculateDistortion.scoresResult(fundamentalAmplitude, fundamentalAngle, amplitude1, nHarmonics1, angle1,
						amplitude2, nHarmonics2, angle2, amplitude3, nHarmonics3, angle3, amplitude4, nHarmonics4, angle4,
						amplitude5, nHarmonics5, angle5, amplitude6, nHarmonics6, angle6));
			}
	
		}	
	}
}
