package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import model.CalculatePotency;
import view.Graphic;
import view.Initial;

public class ActionFundamentalPower implements ActionListener {

	JFrame framePot;
	JFrame menu;
	Graphic graphVoltage;
	Graphic graphElectricCurrent;
	Graphic graphResult;
	JTextField txtVeff;
	JTextField txtAeff;
	JTextField txtAngleVoltage;
	JTextField txtAngleElectricCurrent;
	JTextField txtPotAtv;
	JTextField txtPotReat;
	JTextField txtPotApar;
	JTextField txtFatPot;
	
	public ActionFundamentalPower(JFrame framePot, Graphic graphVoltage, Graphic graphElectricCurrent, Graphic graphResult, 
			JTextField txtVeff, JTextField txtAeff, JTextField txtAngleVoltage, JTextField txtAngleElectricCurrent,
			JTextField txtPotAtv, JTextField txtPotReat, JTextField txtPotApar, JTextField txtFatPot) {
		this.framePot = framePot;
		this.graphVoltage = graphVoltage;
		this.graphElectricCurrent = graphElectricCurrent;
		this.graphResult = graphResult;
		this.txtVeff = txtVeff;
		this.txtAeff = txtAeff;
		this.txtAngleVoltage = txtAngleVoltage;
		this.txtAngleElectricCurrent = txtAngleElectricCurrent;
		this.txtPotAtv = txtPotAtv;
		this.txtPotReat = txtPotReat;
		this.txtPotApar = txtPotApar;
		this.txtFatPot = txtFatPot;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		String comando = e.getActionCommand();
		Double veff;
		Double angleVoltage;
		Double aeff;
		Double angleElectricCurrent;
		
		veff = Double.parseDouble(txtVeff.getText());
		angleVoltage = Double.parseDouble(txtAngleVoltage.getText());
		aeff = Double.parseDouble(txtAeff.getText());
		angleElectricCurrent = Double.parseDouble(txtAngleElectricCurrent.getText());
		
		if(comando.equals("Ok") && (veff>220 || veff<0)) {
			JOptionPane.showMessageDialog(framePot,
			        "Veff deve estar entre 0 e 220!", 
			        "Error", 
			        JOptionPane.ERROR_MESSAGE);
		}
		
		if(comando.equals("Ok") && (aeff>220 || aeff<0)) {
			JOptionPane.showMessageDialog(framePot,
			        "Aeff deve estar entre 0 e 220!", 
			        "Error", 
			        JOptionPane.ERROR_MESSAGE);
		}
		
		if(comando.equals("Ok") && ((angleVoltage>180 || angleVoltage<-180) || (angleElectricCurrent>180 || angleElectricCurrent<-180))) {
			JOptionPane.showMessageDialog(framePot,
			        "Ângulo de fase deve estar entre -180 e 180!", 
			        "Error", 
			        JOptionPane.ERROR_MESSAGE);
		}
		
		else if(comando.equals("Ok") && comando.equals("Ok") && (veff<=220 && veff>=0) && (angleElectricCurrent<=180 && angleElectricCurrent>=-180) && (aeff<=220 && aeff>=0) && (angleVoltage<=180 && angleVoltage>=-180)){	
	
			List<Double> voltageScores = new ArrayList<>();
			List<Double> electricCurrentScores = new ArrayList<>();

			voltageScores = CalculatePotency.scoresVoltage(veff, angleVoltage);
			electricCurrentScores = CalculatePotency.scoresElectricCurrent(aeff, angleElectricCurrent);
		
			graphVoltage.setScores(voltageScores);
			graphElectricCurrent.setScores(electricCurrentScores);
			graphResult.setScores(CalculatePotency.scoresResult(voltageScores, electricCurrentScores));
		
			txtPotAtv.setText(Integer.toString(CalculatePotency.voltageActived(aeff, veff, angleVoltage, angleElectricCurrent)) + " Watt");
			txtPotReat.setText(Integer.toString(CalculatePotency.voltageReactived(aeff, veff, angleVoltage, angleElectricCurrent)) + " VAR");
			txtPotApar.setText(Integer.toString(CalculatePotency.voltageApparent(aeff, veff)) + " VAR");
			txtFatPot.setText(String.format("%.2f",CalculatePotency.factorVoltage(angleVoltage, angleElectricCurrent)));
		}
		
		if(comando.equals("Ok") && (veff<=220 && veff>=0) && (angleElectricCurrent<=180 && angleElectricCurrent>=-180) && (aeff<=220 && aeff>=0) && (angleVoltage<=180 && angleVoltage>=-180)) {
			List<Double> voltageScores = new ArrayList<>();

			voltageScores = CalculatePotency.scoresVoltage(veff, angleVoltage);
			
			graphVoltage.setScores(voltageScores);
		}
		
		if(comando.equals("Ok") && (veff<=220 && veff>=0) && (angleElectricCurrent<=180 && angleElectricCurrent>=-180) && (aeff<=220 && aeff>=0) && (angleVoltage<=180 && angleVoltage>=-180)) {
			List<Double> electricCurrentScores = new ArrayList<>();

			electricCurrentScores = CalculatePotency.scoresElectricCurrent(aeff, angleVoltage);
			
			graphElectricCurrent.setScores(electricCurrentScores);
		}
		
		else if(comando.equals("Return")){
			framePot.dispose();
			menu = new Initial();
			menu.setVisible(true);
		}
	}

}
