package view;

import java.awt.EventQueue;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import controllers.ActionInitial;
import java.awt.Font;

public class Initial extends JFrame implements GraphicInterface{

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
					new Initial().setVisible(true);
			}
		});
	}

	public Initial() {
		initComponents();
	}

	public void initComponents() { 

		JPanel simulations = new javax.swing.JPanel();
		setContentPane(simulations);
		
		setTitle("Aprenda+ QEE!");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);

		setBounds(100, 100, 447, 234);
		simulations.setBorder(new EmptyBorder(5, 5, 5, 5));
		simulations.setLayout(null);
		
		JLabel lblLearnQee = new JLabel("Aprenda+ QEE!");
		lblLearnQee.setFont(new Font("Arial", Font.ITALIC, 29));
		lblLearnQee.setBounds(186, 102, 208, 52);
		simulations.add(lblLearnQee);
		
		JButton btPotFund = new JButton("Simular Fluxo de Potência Fundamental");
		btPotFund.setBounds(149, 190, 299, 29);
		simulations.add(btPotFund);
		
		JButton btDistHarm = new JButton("Simular Distorção Harmônica");
		btDistHarm.setBounds(149, 251, 299, 29);
		simulations.add(btDistHarm);
			
		JButton btExit = new JButton("Sair");
		btExit.setBounds(445, 598, 117, 29);
		simulations.add(btExit);
		
		btPotFund.setActionCommand("PotFund");
		btDistHarm.setActionCommand("DistHarm");
		btExit.setActionCommand("Exit");
		
		btPotFund.addActionListener(new ActionInitial(this));
		btDistHarm.addActionListener(new ActionInitial(this));
		btExit.addActionListener(new ActionInitial(this));
		
		setLocationRelativeTo(null);
		setSize(600,680);
		setVisible(true);
		
	}
}
