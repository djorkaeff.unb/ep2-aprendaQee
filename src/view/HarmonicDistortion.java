package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;

import com.sun.beans.editors.NumberEditor;

import controllers.ActionHarmonicDistortion;
import controllers.ActionInitial;
import model.CalculateDistortion;
import model.CalculatePotency;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JRadioButton;
import java.util.List;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JSpinner;

public class HarmonicDistortion extends JFrame implements GraphicInterface{

	private JPanel contentPane;
	private JTextField txtAmplitudeFundComp;
	private JTextField txtAngleFundComp;
	private JTextField txtNumberHarmonics;
	private JLabel lblFinalResult;
	
	public HarmonicDistortion() {
		initComponents();
	}
	
	public void initComponents() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(570, 770);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		ButtonGroup buttonGroup = new ButtonGroup();
		
		PanelHarmonics harmonicDistortion = new PanelHarmonics();
		JScrollPane scrollPane = new JScrollPane(harmonicDistortion);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(0, 220, 570, 280);

		contentPane.add(scrollPane);
		
		JLabel lblEntrys = new JLabel("Entradas");
		lblEntrys.setFont(new Font("Lucida Grande", Font.PLAIN, 18));
		lblEntrys.setBounds(253, 6, 92, 32);
		contentPane.add(lblEntrys);
		
		JRadioButton rdOdd = new JRadioButton("Ímpares");
		rdOdd.setBounds(439, 60, 100, 23);
		contentPane.add(rdOdd);
		
		JRadioButton rdEven = new JRadioButton("Pares");
		rdEven.setBounds(439, 95, 100, 23);
		contentPane.add(rdEven);
		
		buttonGroup.add(rdOdd);
		buttonGroup.add(rdEven);
		
		JLabel lblHarmonics = new JLabel("Harmônicos");
		lblHarmonics.setBounds(445, 32, 100, 16);
		contentPane.add(lblHarmonics);
		
		JLabel lblFundamentalComponent = new JLabel("Componente Fundamental");
		lblFundamentalComponent.setBounds(127, 52, 179, 16);
		contentPane.add(lblFundamentalComponent);
		
		JLabel lblAmplitude = new JLabel("Amplitude: ");
		lblAmplitude.setBounds(61, 177, 92, 16);
		contentPane.add(lblAmplitude);
		
		txtAmplitudeFundComp = new JTextField();
		txtAmplitudeFundComp.setHorizontalAlignment(SwingConstants.CENTER);
		txtAmplitudeFundComp.setText("0");
		txtAmplitudeFundComp.setBounds(138, 172, 65, 26);
		contentPane.add(txtAmplitudeFundComp);
		txtAmplitudeFundComp.setColumns(10);
		
		JLabel lblAngle = new JLabel("Âng. de Fase:");
		lblAngle.setBounds(224, 177, 92, 16);
		contentPane.add(lblAngle);
		
		txtAngleFundComp = new JTextField();
		txtAngleFundComp.setHorizontalAlignment(SwingConstants.CENTER);
		txtAngleFundComp.setText("0");
		txtAngleFundComp.setBounds(315, 172, 65, 26);
		contentPane.add(txtAngleFundComp);
		txtAngleFundComp.setColumns(10);
		
		JLabel lblNumberHarmonics = new JLabel("Núm. de ordens harmônicas");
		lblNumberHarmonics.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
		lblNumberHarmonics.setBounds(412, 130, 158, 16);
		contentPane.add(lblNumberHarmonics);
		
		JButton btOkNumberHarmonics = new JButton("OK");
		btOkNumberHarmonics.setBounds(445, 179, 79, 29);
		contentPane.add(btOkNumberHarmonics);
		
		JLabel lblExits = new JLabel("Saídas");
		lblExits.setFont(new Font("Lucida Grande", Font.PLAIN, 18));
		lblExits.setBounds(256, 503, 61, 23);
		contentPane.add(lblExits);
		
		JLabel lblResult = new JLabel("Resultante");
		lblResult.setBounds(251, 538, 108, 16);
		contentPane.add(lblResult);
		
		Graphic graphicResult = new Graphic(CalculatePotency.scoresInit(), 3);
		graphicResult.setBounds(70, 552, 388, 80);
		contentPane.add(graphicResult);
		
		JLabel lblSerie = new JLabel("Série de Fourier Amplitude-Fase");
		lblSerie.setBounds(183, 652, 221, 16);
		contentPane.add(lblSerie);
		
		lblFinalResult = new JLabel("\"Série Resultante\"");
		lblFinalResult.setHorizontalAlignment(SwingConstants.CENTER);
		lblFinalResult.setFont(new Font("Lucida Grande", Font.ITALIC, 13));
		lblFinalResult.setBounds(28, 680, 519, 16);
		contentPane.add(lblFinalResult);
		
		JButton btnGerarResultado = new JButton("Gerar Resultado");
		btnGerarResultado.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
		btnGerarResultado.setBounds(428, 702, 117, 29);
		contentPane.add(btnGerarResultado);
		
		Graphic fundamentalComponent = new Graphic(CalculateDistortion.scoresInit(), 1);
		fundamentalComponent.setBounds(6, 68, 394, 80);
		contentPane.add(fundamentalComponent);
		
		btOkNumberHarmonics.setActionCommand("OkHarmonics");
		btnGerarResultado.setActionCommand("Result");
		
		JSpinner spinner = new JSpinner();
		SpinnerNumberModel nHarm = new SpinnerNumberModel((Integer) 0,(Integer) 0, (Integer) 6,(Integer) 1);
		spinner.setModel(nHarm);
		
		JFormattedTextField tf = ((JSpinner.DefaultEditor) spinner.getEditor()).getTextField();
		tf.setEditable(false);	
		spinner.setBounds(460, 147, 50, 26);
		contentPane.add(spinner);
		
		btOkNumberHarmonics.addActionListener(new ActionHarmonicDistortion(harmonicDistortion, spinner, fundamentalComponent, txtAmplitudeFundComp, txtAngleFundComp, lblFinalResult, graphicResult));
		btnGerarResultado.addActionListener(new ActionHarmonicDistortion(harmonicDistortion, spinner, fundamentalComponent, txtAmplitudeFundComp, txtAngleFundComp, lblFinalResult, graphicResult));
	}
}
