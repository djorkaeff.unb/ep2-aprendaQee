package view;

import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import controllers.ActionFundamentalPower;
import model.CalculatePotency;

public class FundamentalPower extends JFrame implements GraphicInterface{

	private JTextField txtVeff;
	private JTextField txtAeff;
	private JTextField txtAngleVoltage;
	private JTextField txtAngleElectricCurrent;
	private JTextField txtPotAtv;
	private JTextField txtPotReat;
	private JTextField txtPotApar;
	private JTextField txtFatPot;
	private JButton btnOkEntrys;
	private JButton btnReturn;

	public FundamentalPower() {
		
		initComponents();
		
		Graphic graphVoltage = new Graphic(CalculatePotency.scoresInit(), 1);
		graphVoltage.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				JFrame frameGraph = new JFrame();
				frameGraph.setLocation(100,100);
				frameGraph.setTitle("Gráfico de Tensão");
				frameGraph.setResizable(false);
				frameGraph.setSize(700, 350);
				frameGraph.setContentPane(new GraphFrame(CalculatePotency.scoresVoltage(Double.parseDouble(txtVeff.getText()), Double.parseDouble(txtAngleVoltage.getText())), 1));
				frameGraph.setVisible(true);
			}
		});
		graphVoltage.setBounds(-11, 66, 388, 80);
		getContentPane().add(graphVoltage);
		
		Graphic graphElectricCurrent = new Graphic(CalculatePotency.scoresInit(), 2);
		graphElectricCurrent.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				JFrame frameGraph = new JFrame();
				frameGraph.setLocation(100,100);
				frameGraph.setTitle("Gráfico de Corrente");
				frameGraph.setResizable(false);
				frameGraph.setSize(700, 350);
				frameGraph.setContentPane(new GraphFrame(CalculatePotency.scoresElectricCurrent(Double.parseDouble(txtAeff.getText()), Double.parseDouble(txtAngleElectricCurrent.getText())), 2));
				frameGraph.setVisible(true);
			}
		});
		graphElectricCurrent.setBounds(-11, 196, 388, 80);
		getContentPane().add(graphElectricCurrent);
		
		Graphic graphResult = new Graphic(CalculatePotency.scoresInit(), 3);
		graphResult.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				JFrame frameGraph = new JFrame();
				frameGraph.setLocation(100,100);
				frameGraph.setTitle("Gráfico de Potência Instantânea");
				frameGraph.setResizable(false);
				frameGraph.setSize(700, 350);
				frameGraph.setContentPane(new GraphFrame(CalculatePotency.scoresResult(CalculatePotency.scoresVoltage(Double.parseDouble(txtVeff.getText()), Double.parseDouble(txtAngleVoltage.getText())), 
						CalculatePotency.scoresElectricCurrent(Double.parseDouble(txtAeff.getText()), Double.parseDouble(txtAngleElectricCurrent.getText()))), 3));
				frameGraph.setVisible(true);
			}
		});
		graphResult.setBounds(-11, 422, 388, 80);
		getContentPane().add(graphResult);
		
		setResizable(false);
		setVisible(true);
		
		btnOkEntrys.setActionCommand("Ok");
		btnReturn.setActionCommand("Return");
		
		btnOkEntrys.addActionListener(new ActionFundamentalPower(this, graphVoltage, graphElectricCurrent, graphResult, 
				txtVeff, txtAeff, txtAngleVoltage, txtAngleElectricCurrent, txtPotAtv, txtPotReat, txtPotApar, txtFatPot));
		
		btnReturn.addActionListener(new ActionFundamentalPower(this, graphVoltage, graphElectricCurrent, graphResult, 
			txtVeff, txtAeff, txtAngleVoltage, txtAngleElectricCurrent, txtPotAtv, txtPotReat, txtPotApar, txtFatPot));
	}
	
	public void initComponents() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(200, 200, 605, 678);
		getContentPane().setLayout(null);
		setLocationRelativeTo(null);
		setTitle("Fluxo de Potência Fundamental");
		
		JLabel lblVoltage = new JLabel("Tensão");
		lblVoltage.setFont(new Font("Arial", Font.PLAIN, 12));
		lblVoltage.setBounds(171, 53, 61, 16);
		getContentPane().add(lblVoltage);
		
		JLabel lblEntrys = new JLabel("Entradas");
		lblEntrys.setFont(new Font("Arial", Font.PLAIN, 20));
		lblEntrys.setBounds(267, 6, 118, 24);
		getContentPane().add(lblEntrys);
		
		btnOkEntrys = new JButton("OK");
		btnOkEntrys.setBounds(270, 298, 75, 29);
		getContentPane().add(btnOkEntrys);
		
		btnReturn = new JButton("Voltar");
		btnReturn.setBounds(468, 612, 117, 29);
		getContentPane().add(btnReturn);
		
		JLabel lblElectricCurrent = new JLabel("Corrente");
		lblElectricCurrent.setFont(new Font("Arial", Font.PLAIN, 12));
		lblElectricCurrent.setBounds(171, 182, 61, 16);
		getContentPane().add(lblElectricCurrent);
		
		JSeparator separator1 = new JSeparator();
		separator1.setBounds(0, 342, 614, 12);
		getContentPane().add(separator1);
		
		JSeparator separator4 = new JSeparator();
		separator4.setBounds(0, 388, 614, 12);
		getContentPane().add(separator4);
		
		JSeparator separator3 = new JSeparator();
		separator3.setBounds(0, 32, 614, 12);
		getContentPane().add(separator3);
		
		JLabel lblExits = new JLabel("Saídas");
		lblExits.setFont(new Font("Arial", Font.PLAIN, 20));
		lblExits.setBounds(278, 361, 100, 24);
		getContentPane().add(lblExits);
			
		JLabel lblVoltageSnapshot = new JLabel("Potência Instantânea");
		lblVoltageSnapshot.setFont(new Font("Arial", Font.PLAIN, 12));
		lblVoltageSnapshot.setBounds(141, 406, 154, 16);
		getContentPane().add(lblVoltageSnapshot);
		
		JLabel lblVoltageActivated = new JLabel("Potência Ativa: ");
		lblVoltageActivated.setBounds(16, 534, 118, 16);
		getContentPane().add(lblVoltageActivated);
		
		JLabel lblVoltageReactive = new JLabel("Potência Reativa:");
		lblVoltageReactive.setBounds(16, 562, 118, 16);
		getContentPane().add(lblVoltageReactive);
		
		JLabel lblVoltageApparent = new JLabel("Potência Aparente:");
		lblVoltageApparent.setBounds(16, 590, 118, 16);
		getContentPane().add(lblVoltageApparent);
		
		JLabel lblFactor = new JLabel("Fator de Potência:");
		lblFactor.setBounds(16, 617, 123, 16);
		getContentPane().add(lblFactor);
		
		txtVeff = new JTextField();
		txtVeff.setHorizontalAlignment(SwingConstants.CENTER);
		txtVeff.setText("0"); // TENSAO
		txtVeff.setBounds(510, 70, 75, 26);
		getContentPane().add(txtVeff);
		txtVeff.setColumns(10);
		
		txtAngleVoltage = new JTextField();
		txtAngleVoltage.setHorizontalAlignment(SwingConstants.CENTER);
		txtAngleVoltage.setText("0"); // ANGULO DA TENSAO
		txtAngleVoltage.setBounds(510, 120, 75, 26);
		getContentPane().add(txtAngleVoltage);
		txtAngleVoltage.setColumns(10);
	
		txtAeff = new JTextField();
		txtAeff.setHorizontalAlignment(SwingConstants.CENTER);
		txtAeff.setText("0"); // CORRENTE
		txtAeff.setBounds(510, 196, 75, 26);
		getContentPane().add(txtAeff);
		txtAeff.setColumns(10);
		
		txtAngleElectricCurrent = new JTextField();
		txtAngleElectricCurrent.setHorizontalAlignment(SwingConstants.CENTER);
		txtAngleElectricCurrent.setText("0"); // ANGULO DA CORRENTE
		txtAngleElectricCurrent.setBounds(510, 247, 75, 26);
		getContentPane().add(txtAngleElectricCurrent);
		txtAngleElectricCurrent.setColumns(10);
		
		JLabel lblVeff = new JLabel("Veff:");
		lblVeff.setBounds(476, 75, 61, 16);
		getContentPane().add(lblVeff);
		
		JLabel lblAngleVoltage = new JLabel("Ângulo de fase:");
		lblAngleVoltage.setBounds(408, 125, 107, 16);
		getContentPane().add(lblAngleVoltage);
		
		JLabel lblAngleElectricCurrent = new JLabel("Ângulo de fase:");
		lblAngleElectricCurrent.setBounds(408, 252, 107, 16);
		getContentPane().add(lblAngleElectricCurrent);
		
		JLabel lblAeff = new JLabel("Aeff:");
		lblAeff.setBounds(476, 201, 61, 16);
		getContentPane().add(lblAeff);
		
		txtPotAtv = new JTextField();
		txtPotAtv.setEditable(false);
		txtPotAtv.setHorizontalAlignment(SwingConstants.CENTER);
		txtPotAtv.setText(" Watt");
		txtPotAtv.setBounds(141, 529, 130, 26);
		getContentPane().add(txtPotAtv);
		txtPotAtv.setColumns(10);
		
		txtPotReat = new JTextField();
		txtPotReat.setEditable(false);
		txtPotReat.setHorizontalAlignment(SwingConstants.CENTER);
		txtPotReat.setText(" VAR"); // CONTA
		txtPotReat.setBounds(141, 557, 130, 26);
		getContentPane().add(txtPotReat);
		txtPotReat.setColumns(10);
		
		txtPotApar = new JTextField();
		txtPotApar.setEditable(false);
		txtPotApar.setHorizontalAlignment(SwingConstants.CENTER);
		txtPotApar.setText(" VAR"); // CONTA
		txtPotApar.setBounds(141, 585, 130, 26);
		getContentPane().add(txtPotApar);
		txtPotApar.setColumns(10);
		
		txtFatPot = new JTextField();
		txtFatPot.setEditable(false);
		txtFatPot.setHorizontalAlignment(SwingConstants.CENTER);
		txtFatPot.setText(""); // CONTA
		txtFatPot.setBounds(141, 612, 130, 26);
		getContentPane().add(txtFatPot);
		txtFatPot.setColumns(10);
	}
}
