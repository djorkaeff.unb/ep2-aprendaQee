package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.CalculatePotency;

import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import java.awt.Dimension;
import java.util.List;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerNumberModel;

public class PanelHarmonics extends JPanel implements GraphicInterface{

	public JTextField txtAmplitude1;
	public JTextField txtAngle1;
	public JTextField txtAmplitude2;
	public JTextField txtAngle2;
	public JTextField txtAmplitude3;
	public JTextField txtAngle3;
	public JTextField txtAmplitude4;
	public JTextField txtAngle4;
	public JTextField txtAmplitude5;
	public JTextField txtAngle5;
	public JTextField txtAmplitude6;
	public JTextField txtAngle6;
	
	public JSpinner spinner1;
	public JSpinner spinner2;
	public JSpinner spinner3;
	public JSpinner spinner4;
	public JSpinner spinner5;
	public JSpinner spinner6;
	
	public JTextField txtOrder1;
	public JTextField txtOrder2;
	public JTextField txtOrder3;
	public JTextField txtOrder4;
	public JTextField txtOrder5;
	public JTextField txtOrder6;
	
	public Graphic graphic1;
	public Graphic graphic2;
	public Graphic graphic3;
	public Graphic graphic4;
	public Graphic graphic5;
	public Graphic graphic6;

	public PanelHarmonics() {
		initComponents();
	}
	
	public void initComponents() {
		setLayout(null);
		setPreferredSize(new Dimension(555,840));
		
		graphic1 = new Graphic(CalculatePotency.scoresInit(), 2);
		graphic1.setBounds(10, 10, 387, 87);
		add(graphic1);
		
		JLabel lblAmplitude1 = new JLabel("Amplitude:");
		lblAmplitude1.setBounds(63, 114, 77, 16);
		add(lblAmplitude1);
		
		JLabel lblAngle1 = new JLabel("Âng. de Fase:");
		lblAngle1.setBounds(226, 114, 89, 16);
		add(lblAngle1);
		
		txtAmplitude1 = new JTextField();
		txtAmplitude1.setHorizontalAlignment(SwingConstants.CENTER);
		txtAmplitude1.setText("0");
		txtAmplitude1.setEditable(false);
		txtAmplitude1.setBounds(135, 109, 65, 26);
		add(txtAmplitude1);
		txtAmplitude1.setColumns(10);
		
		txtAngle1 = new JTextField();
		txtAngle1.setHorizontalAlignment(SwingConstants.CENTER);
		txtAngle1.setText("0");
		txtAngle1.setEditable(false);
		txtAngle1.setBounds(314, 109, 65, 26);
		add(txtAngle1);
		txtAngle1.setColumns(10);
		
		graphic2 = new Graphic(CalculatePotency.scoresInit(), 2);
		graphic2.setBounds(10, 147, 387, 78);
		add(graphic2);
		
		JLabel lblAmplitude2 = new JLabel("Amplitude:");
		lblAmplitude2.setBounds(63, 251, 77, 16);
		add(lblAmplitude2);
		
		JLabel lblAngle2 = new JLabel("Âng. de Fase:");
		lblAngle2.setBounds(226, 251, 89, 16);
		add(lblAngle2);
		
		txtAmplitude2 = new JTextField();
		txtAmplitude2.setText("0");
		txtAmplitude2.setEditable(false);
		txtAmplitude2.setHorizontalAlignment(SwingConstants.CENTER);
		txtAmplitude2.setColumns(10);
		txtAmplitude2.setBounds(135, 246, 65, 26);
		add(txtAmplitude2);
		
		txtAngle2 = new JTextField();
		txtAngle2.setText("0");
		txtAngle2.setEditable(false);
		txtAngle2.setHorizontalAlignment(SwingConstants.CENTER);
		txtAngle2.setColumns(10);
		txtAngle2.setBounds(314, 246, 65, 26);
		add(txtAngle2);
		
		graphic3 = new Graphic(CalculatePotency.scoresInit(), 2);
		graphic3.setBounds(10, 284, 387, 78);
		add(graphic3);
		
		JLabel lblAmplitude3 = new JLabel("Amplitude:");
		lblAmplitude3.setBounds(63, 388, 77, 16);
		add(lblAmplitude3);
		
		JLabel lblAngle3 = new JLabel("Âng. de Fase:");
		lblAngle3.setBounds(226, 388, 89, 16);
		add(lblAngle3);
		
		txtAmplitude3 = new JTextField();
		txtAmplitude3.setText("0");
		txtAmplitude3.setEditable(false);
		txtAmplitude3.setHorizontalAlignment(SwingConstants.CENTER);
		txtAmplitude3.setColumns(10);
		txtAmplitude3.setBounds(135, 383, 65, 26);
		add(txtAmplitude3);
		
		txtAngle3 = new JTextField();
		txtAngle3.setText("0");
		txtAngle3.setEditable(false);
		txtAngle3.setHorizontalAlignment(SwingConstants.CENTER);
		txtAngle3.setColumns(10);
		txtAngle3.setBounds(314, 383, 65, 26);
		add(txtAngle3);
		
		graphic4 = new Graphic(CalculatePotency.scoresInit(), 2);
		graphic4.setBounds(10, 421, 387, 78);
		add(graphic4);
		
		JLabel lblAmplitude4 = new JLabel("Amplitude:");
		lblAmplitude4.setBounds(63, 525, 77, 16);
		add(lblAmplitude4);

		JLabel lblAngle4 = new JLabel("Âng. de Fase:");
		lblAngle4.setBounds(226, 525, 89, 16);
		add(lblAngle4);
		
		txtAmplitude4 = new JTextField();
		txtAmplitude4.setText("0");
		txtAmplitude4.setEditable(false);
		txtAmplitude4.setHorizontalAlignment(SwingConstants.CENTER);
		txtAmplitude4.setColumns(10);
		txtAmplitude4.setBounds(135, 520, 65, 26);
		add(txtAmplitude4);
		
		txtAngle4 = new JTextField();
		txtAngle4.setText("0");
		txtAngle4.setEditable(false);
		txtAngle4.setHorizontalAlignment(SwingConstants.CENTER);
		txtAngle4.setColumns(10);
		txtAngle4.setBounds(314, 520, 65, 26);
		add(txtAngle4);
		
		graphic5 = new Graphic(CalculatePotency.scoresInit(), 2);
		graphic5.setBounds(10, 561, 387, 78);
		add(graphic5);
		
		JLabel lblAmplitude5 = new JLabel("Amplitude:");
		lblAmplitude5.setBounds(63, 665, 77, 16);
		add(lblAmplitude5);
		
		JLabel lblAngle5 = new JLabel("Âng. de Fase:");
		lblAngle5.setBounds(226, 665, 89, 16);
		add(lblAngle5);
		
		txtAmplitude5 = new JTextField();
		txtAmplitude5.setText("0");
		txtAmplitude5.setEditable(false);
		txtAmplitude5.setHorizontalAlignment(SwingConstants.CENTER);
		txtAmplitude5.setColumns(10);
		txtAmplitude5.setBounds(135, 660, 65, 26);
		add(txtAmplitude5);
		
		txtAngle5 = new JTextField();
		txtAngle5.setText("0");
		txtAngle5.setEditable(false);
		txtAngle5.setHorizontalAlignment(SwingConstants.CENTER);
		txtAngle5.setColumns(10);
		txtAngle5.setBounds(314, 660, 65, 26);
		add(txtAngle5);
		
		graphic6 = new Graphic(CalculatePotency.scoresInit(), 2);
		graphic6.setBounds(10, 698, 387, 78);
		add(graphic6);
		
		JLabel lblAmplitude6 = new JLabel("Amplitude:");
		lblAmplitude6.setBounds(63, 802, 77, 16);
		add(lblAmplitude6);
		
		JLabel lblAngle6 = new JLabel("Âng. de Fase:");
		lblAngle6.setBounds(226, 802, 89, 16);
		add(lblAngle6);
		
		txtAmplitude6 = new JTextField();
		txtAmplitude6.setText("0");
		txtAmplitude6.setEditable(false);
		txtAmplitude6.setHorizontalAlignment(SwingConstants.CENTER);
		txtAmplitude6.setColumns(10);
		txtAmplitude6.setBounds(135, 797, 65, 26);
		add(txtAmplitude6);
		
		txtAngle6 = new JTextField();
		txtAngle6.setText("0");
		txtAngle6.setEditable(false);
		txtAngle6.setHorizontalAlignment(SwingConstants.CENTER);
		txtAngle6.setColumns(10);
		txtAngle6.setBounds(314, 797, 65, 26);
		add(txtAngle6);
		
		JLabel lblOrder1 = new JLabel("Ordem Harmônica");
		lblOrder1.setBounds(420, 33, 114, 16);
		add(lblOrder1);
		
		JLabel lblOrder2 = new JLabel("Ordem Harmônica");
		lblOrder2.setBounds(420, 170, 114, 16);
		add(lblOrder2);
		
		JLabel lblOrder3 = new JLabel("Ordem Harmônica");
		lblOrder3.setBounds(420, 303, 114, 16);
		add(lblOrder3);
		
		JLabel lblOrder4 = new JLabel("Ordem Harmônica");
		lblOrder4.setBounds(420, 439, 114, 16);
		add(lblOrder4);
		
		JLabel lblOrder5 = new JLabel("Ordem Harmônica");
		lblOrder5.setBounds(420, 582, 114, 16);
		add(lblOrder5);
		
		JLabel lblOrder6 = new JLabel("Ordem Harmônica");
		lblOrder6.setBounds(420, 718, 114, 16);
		add(lblOrder6);
		
		SpinnerNumberModel nHarm1 = new SpinnerNumberModel((Integer) 0,(Integer) 0, (Integer) 15,(Integer) 1);
		SpinnerNumberModel nHarm2 = new SpinnerNumberModel((Integer) 0,(Integer) 0, (Integer) 15,(Integer) 1);
		SpinnerNumberModel nHarm3 = new SpinnerNumberModel((Integer) 0,(Integer) 0, (Integer) 15,(Integer) 1);
		SpinnerNumberModel nHarm4 = new SpinnerNumberModel((Integer) 0,(Integer) 0, (Integer) 15,(Integer) 1);
		SpinnerNumberModel nHarm5 = new SpinnerNumberModel((Integer) 0,(Integer) 0, (Integer) 15,(Integer) 1);
		SpinnerNumberModel nHarm6 = new SpinnerNumberModel((Integer) 0,(Integer) 0, (Integer) 15,(Integer) 1);
		
		spinner1 = new JSpinner();
		spinner1.setModel(nHarm1);
		spinner1.setBounds(448, 53, 65, 26);
		add(spinner1);	
		JFormattedTextField tf1 = ((JSpinner.DefaultEditor) spinner1.getEditor()).getTextField();
		tf1.setEditable(false);
		
		spinner2 = new JSpinner();
		spinner2.setModel(nHarm2);
		spinner2.setBounds(448, 190, 65, 26);
		add(spinner2);	
		JFormattedTextField tf2 = ((JSpinner.DefaultEditor) spinner2.getEditor()).getTextField();
		tf2.setEditable(false);
		
		spinner3 = new JSpinner();
		spinner3.setModel(nHarm3);
		spinner3.setBounds(448, 323, 65, 26);
		add(spinner3);	
		JFormattedTextField tf3 = ((JSpinner.DefaultEditor) spinner3.getEditor()).getTextField();
		tf3.setEditable(false);
		
		spinner4 = new JSpinner();
		spinner4.setModel(nHarm4);
		spinner4.setBounds(448, 459, 65, 26);
		add(spinner4);	
		JFormattedTextField tf4 = ((JSpinner.DefaultEditor) spinner4.getEditor()).getTextField();
		tf4.setEditable(false);
		
		spinner5 = new JSpinner();
		spinner5.setModel(nHarm5);
		spinner5.setBounds(448, 602, 65, 26);
		add(spinner5);	
		JFormattedTextField tf5 = ((JSpinner.DefaultEditor) spinner5.getEditor()).getTextField();
		tf5.setEditable(false);
		
		spinner6 = new JSpinner();
		spinner6.setModel(nHarm6);
		spinner6.setBounds(448, 738, 65, 26);
		add(spinner6);	
		JFormattedTextField tf6 = ((JSpinner.DefaultEditor) spinner6.getEditor()).getTextField();
		tf6.setEditable(false);
		
	}
}
